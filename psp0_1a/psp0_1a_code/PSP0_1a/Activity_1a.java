
import java.util.*;
import java.io.IOException;
/**
 *
 */
public class Activity_1a {

	/**
	 * Default constructor
	 */
	public Activity_1a() {
	}

	/**
	 *
	 */
	public static ArrayList<Double> dataset;

	/**
	 *
	 */
	public static double mean;

	/**
	 *
	 */
	public static double standardDeviation;

	/**
	 * @return
	 */
	public static void main(String[] args) throws IOException{
		// TODO implement here
		try{
			Scanner input = new Scanner(System.in);
			System.out.println("Por favor, indique el nombre del archivo de donde recuperar los datos.");
			String name = input.next();
			//Importar dataset y convertirlo
			dataset = parseData(name);

			//Obtener promedio
			if(dataset.isEmpty()){
				//Salir
				System.out.println("Parece que el set de datos está vacío, intente con un archivo válido");
				System.exit(0);
			}
			mean = getMean(dataset);
			//Obtener desviación estandard
			standardDeviation = getStd(dataset, mean);

			//Desplegar información
			System.out.println("Promedio: " + mean);
			System.out.println("Desviación estandard: " + standardDeviation);
		}
		catch(Exception e){
			System.out.println("Ocurrió un error. Intente correr el programa de nuevo.");
		}
	}

	/**
	 * @return
	 */
	public static ArrayList<Double> parseData(String name) throws IOException{
		// TODO implement here
		ReadFile file = new ReadFile(name);
		String[] lines = file.OpenFile();
		ArrayList<Double> lista = new ArrayList<Double>();
		//Convertir a double
		for(int i = 0; i < lines.length; i++){
			lista.add(Double.parseDouble(lines[i]));
		}

		for(int i = 0; i < lista.size(); i++){
			System.out.println(lista.get(i));
		}
		return lista;
	}

	/**
	 * @param dataset
	 * @return
	 */
	public static double getMean(ArrayList<Double> dataset) {
		// TODO implement here
		Mean mean = new Mean();
		return mean.mean(dataset);
	}

	/**
	 * @param dataset
	 * @param mean
	 * @return
	 */
	public static double getStd(ArrayList<Double> dataset, double mean) {
		// TODO implement here
		StandardDeviation std = new StandardDeviation();
		return std.std(dataset, mean);
	}

}
