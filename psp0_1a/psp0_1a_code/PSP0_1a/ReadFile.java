
import java.util.*;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
/**
 *
 */
public class ReadFile {

	public String path;

	/**
	 * Default constructor
	 */
	public ReadFile(String file_path) {
		path = file_path;
	}

	/**
	 *
	 */

	/**
	 * @return
	 */
	public String[] OpenFile() throws IOException{
		// TODO implement here
		FileReader fr = new FileReader(path);
		BufferedReader textReader = new BufferedReader(fr);
		int numberOfLines = readLines();
		String[] textData = new String[numberOfLines];

		for(int i = 0; i < numberOfLines; i++){
			textData[i] = textReader.readLine();
		}

		textReader.close();
		return textData;
	}

	/**
	 * @return
	 */
	public int readLines() throws IOException{
		// TODO implement here
		FileReader file_to_read = new FileReader(path);
    BufferedReader bf = new BufferedReader(file_to_read);

    String aLine;
    int numberOfLines = 0;

    while((aLine = bf.readLine()) != null){
      numberOfLines++;
    }

    bf.close();

    return numberOfLines;
	}

	//Parsea el arreglo de strings a un arraylist del tipo conveniente
	//Tiene mucha utilidad si se leen numeros
	public <T> ArrayList<T> toArrayList(String[] s) throws IOException{
		ArrayList<T> ar = new ArrayList<T>();

		for(int i = 0; i < s.length; i++){
			ar.add((T)s[i]);
		}

		return ar;
	}

}
