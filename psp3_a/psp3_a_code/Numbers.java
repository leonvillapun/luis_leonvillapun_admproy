/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leonvillapun
 */
import java.io.File;
import java.io.IOException;
import java.util.*;

public class Numbers {
    static Scanner e = new Scanner(System.in);
    static String fileName = "prueba.txt";
    static WriteFile wfile;
    static LinkedList<Double> numbers = new LinkedList<Double>();
    static int totalnum;
    static double mean, sd;

    public static void main(String[] args) throws IOException{

        try{
            int ins = -1;
            numbers.clear();
            System.out.println("Select read or write mode(0/1)");
            int read_or_write = e.nextInt();
            if(read_or_write == 0){ //read mode

               //ReadFile is called to return an array of the lines from txt source.
                ReadFile file = new ReadFile(fileName);
                String[] aryLines = file.OpenFile();
                totalnum = 0;

                if(aryLines.length > 0){ //Checks if input file is empty or not
                    for(int i = 0; i < aryLines.length; i++){
                        System.out.println("Value "+ i + ": " + aryLines[i]);
                        numbers.add(Double.parseDouble(aryLines[i]));
                    }
                }
                else{
                    System.out.println("File is empty.");
                    menu(0);
                }
                menu(ins);
            }
            else{ //write mode
                numbers.clear();

                wfile = new WriteFile(fileName, true); //I'm changing this line
                int a = 0;
                wfile.reset();


                //Fill the numbers
                System.out.println("Number of numbers to input: ");
                int nums = e.nextInt();
                for(int i = 0; i < nums; i++){
                    double act = e.nextDouble();
                    numbers.add(act);
                }

                //Write this in expected file

                for(int i = 0; i < numbers.size(); i++){
                    wfile.writeToFile(numbers.get(i).toString());
                }
                menu(ins);
            }

        }
        catch(IOException ex){
            System.out.println(ex);
        }
    }

    public static void menu(int ins){
        while(ins != 0){
            System.out.println("1. Mean");
            System.out.println("2. Standard Deviation");
            System.out.println("0. Exit");
            System.out.println();
            System.out.println("INSTRUCTION: ");
            ins = e.nextInt();
            switch(ins){
                case 1:
                    double sum = 0;
                    for(int i = 0; i < numbers.size(); i++){
                        sum += numbers.get(i);
                    }
                    mean = sum/numbers.size();
                    System.out.println("Mean: " + mean);
                break;
                case 2:
                    sum = 0;
                    for(int i = 0; i < numbers.size(); i++){
                        sum += numbers.get(i);
                    }
                    mean = sum/numbers.size();

                    sum = 0;
                    for(int i = 0; i < numbers.size(); i++){
                        sum =(numbers.get(i)-mean)*(numbers.get(i)-mean);
                    }
                    sd = sum/(numbers.size()-1);
                    sd = Math.sqrt(sd);
                    System.out.println("Standard deviation " + sd);
                    sum = 0;
                break;
            }
        }
        System.exit(0);
    }

}
