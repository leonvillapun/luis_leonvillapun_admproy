/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.util.*;
/**
 *
 * @author leonvillapun
 */

/*
Class: FileRead
Author: Luis Alfredo León Villapún
Creation date: 25/03/17
Last modified: 25/03/17
Description: This class receives an input file and reads its' LOCs.
*/
public class FileRead{

    //Global variables
    public static String path, pathNew;
    public static ReadFile file,fileNew;
    public static String[] fileLines, fileLinesNew;
    public static int GENERALCOUNT = 0;
    public static int GENERALCOUNTNEW = 0;
    public static boolean comment = false;
    public static List<String> Objects = new ArrayList<String>();
    public static List<Integer> ObjectsStart = new ArrayList<Integer>();
    public static List<Integer> ObjectsEnd = new ArrayList<Integer>();
    public static List<Integer> ObjectsMethods = new ArrayList<Integer>();
    public static List<Character> BracesOpen = new ArrayList<Character>();
    public static List<Character> BracesClosed = new ArrayList<Character>();
    //End global variables

    public static int[] methodLines() throws IOException{
      try{
        Scanner input = new Scanner(System.in);
        path = "";
        System.out.println("Please enter a program to input: ");
        String newString = input.next();
        while(newString.isEmpty()){
          System.out.println("Please enter a not null input");
          newString = input.next();
        }
          path += newString;
        file = new ReadFile(path);
        fileLines = file.OpenFile();
        int n = countLOC(0, fileLines.length);
        objectCounter();
        for(int i = 0; i < Objects.size(); i++){
          objectEnd(ObjectsStart.get(i));
        }
        int[] res = new int[2];
        System.out.printf("PROGRAM #\tObjectName\tNo. of Methods\tObject LOC\t Total program LOC\n");
        for(int i = 0; i < Objects.size(); i++){
          int myMethods = ObjectsMethods.get(i);
          int linesofMethods = countLOC(ObjectsStart.get(i), ObjectsEnd.get(i));
          System.out.printf("0 \t\t %s \t\t %d \t\t %d \t\t-\n",Objects.get(i),(int)myMethods,linesofMethods);
          res[0] = myMethods;
          res[1] = linesofMethods;
        }
        System.out.printf("0\t\t\t\t\t\t\t\t\t%d\n", countLOC(0,ObjectsEnd.get(0)));


        //System.out.println("Want to test line difference vs a new iteration of this code?: (1 for YES/0 for NO)");

        return res;
        /*int cont = input.nextInt();
        if(cont == 0){
          //Close program
          System.exit(0);
        }
        else{
          //Reset old file
          file = new ReadFile(path);
          fileLines = file.OpenFile();

          //Read file again and compare changes
          pathNew = "exampartial_2_a01322275/";

          String newString2 = input.next();
          while(newString2.isEmpty()){
            System.out.println("Please enter a not null input");
            newString2 = input.next();
          }
          pathNew += newString2;

          fileNew = new ReadFile(pathNew);
          fileLinesNew = fileNew.OpenFile();

          //We now compare LOCs between previous iteration and actual iteration
          System.out.println("Number of deleted lines: ");
          System.out.println(Math.abs(fileLinesNew.length-fileLines.length));

          //Display the changed lines

          int changedLines = 0;
          for(int i = 0; i < fileLinesNew.length;i++){
            if(i < fileLines.length){
              //Check if the other array is still in
              if(fileLinesNew[i].compareTo(fileLines[i]) != 0){
                //The line suffered a change
                System.out.println("Line " + i + "changed from " + fileLines[i] + " to " + fileLinesNew[i]);
                changedLines++;
              }
            }
            else{
              break;
            }
          }
          System.out.println("Number of changed lines: " + changedLines);
        }
        */
      }
      catch(Exception e){
        System.out.println("An error occured. Please rerun de program.");
        return null;
      }
    }



    /*
    CountLOC method.
    This method will calculate the LOC of the program in general terms.
    */
    public static int countLOC(int start, int end){
        int count = 1;
        for(int i = start; i < end; i++){
            //Ignora si la línea empieza con // o /* porque es comentario
            fileLines[i] = fileLines[i].replaceAll("\t",""); //We ignore the tabs and spaces
            fileLines[i] = fileLines[i].replaceAll(" ","");
            if(!fileLines[i].isEmpty() && fileLines[i].length() > 1){ //If the line is not empty and bigger than 1
              if(fileLines[i].charAt(0) == '/' &&
                      (fileLines[i].charAt(1) == '/' || fileLines[i].charAt(1) == '*') && !comment){
                //If the line starts with // of /*
                if(fileLines[i].charAt(0) == '/' && fileLines[i].charAt(1) == '*'){
                  //If the line is /* set comments on so we dont count further lines
                  comment = true;
                }
              }
              else if(fileLines[i].contains("*/") && comment){
                //If closing comment is detected, set comment to false in order to count further lines
                comment = false;
              }
              else if(!comment){
                //If the comment boolean is false, increase counter.
                count++;
              }
            }
            else if(!fileLines[i].isEmpty() && fileLines[i].equals("}")){
              //We should not forget about closing braces.
              count++;
            }
        }
        comment = false;
        return count;
    }

    /*
    ObjectCounter method.
    This method will calculate the number of objects the input file has.
    Lines of start and end will be set also. Then the normal procedure countLOC will be executed.
    */
    public static void objectCounter() throws IOException{
      //Reset any previous modification
      Objects.clear();
      ObjectsStart.clear();
      ObjectsEnd.clear();
      BracesOpen.clear();
      BracesClosed.clear();
      fileLines = file.OpenFile();

      /*Detect key words like "public class" or "public static class"
      */

      for(int i = 0; i < fileLines.length; i++){
        if(!fileLines[i].isEmpty()){
          if(fileLines[i].contains("public") || fileLines[i].contains("private") || fileLines[i].contains("protected")){
            if(fileLines[i].contains("class")){
              //Now we know we are facing an object
              fileLines[i] = fileLines[i].replaceAll("\t","");
              String[] lineAr = fileLines[i].split(" ");
              int j = 0;
              //Search for the name on the line
              while(j < lineAr.length){
                if(!lineAr[j].equals("public") && !lineAr[j].equals("private") &&
                  !lineAr[j].equals("public") && !lineAr[j].equals("class") &&
                  !lineAr[j].equals("static") && !lineAr[j].isEmpty()
                ){
                  //We found it!
                  Objects.add(lineAr[j]);
                  ObjectsStart.add(i);
                  break;
                }
                  //We iterate
                  j++;
              }
            }
          }
        }
      }
    }

    /*
    ObjectEnd method.
    This method will calculate the end line of each object
    Lines of end will be set.
    */
    public static void objectEnd(int start) throws IOException{
      //The plan is to count the opening braces and closing braces
      fileLines = file.OpenFile();
      BracesOpen.clear();
      BracesClosed.clear();
      BracesOpen.add('{');
      int end = start;
      for(int i = start+1; i < fileLines.length+1; i++){
        if(BracesOpen.size() != BracesClosed.size() && i<fileLines.length){
          if(!comment){
            //If it is not a comment, lets see if it is a statement
            if(fileLines[i].contains("{")){
              BracesOpen.add('{');
            }
            if(fileLines[i].contains("}")){
              BracesClosed.add('}');
            }
          }
        }
        else{
          //Set a new end line
          end = i-1;
          break;
        }
      }
      ObjectsEnd.add(end);

      //Lets count the number of methods this objects Headers
      int noOfMethods = 0;
      fileLines = file.OpenFile();
      for(int i = start+1; i < end; i++){
        if(!comment){
          if(!fileLines[i].isEmpty()){
            if(fileLines[i].contains("public") || fileLines[i].contains("private") || fileLines[i].contains("protected")){
              if(!fileLines[i].contains("class")){
                //It will ignore it if it is a class
                if(fileLines[i].contains("{")){
                  //The opening brace indicates it is a method since it contains also an access modifier
                  noOfMethods++;
                }
              }
            }
          }
        }
      }
      ObjectsMethods.add(noOfMethods);
    }

}
