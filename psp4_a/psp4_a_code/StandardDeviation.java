
import java.util.*;

/**
 *
 */
public class StandardDeviation {

	/**
	 * Default constructor
	 */
	public StandardDeviation(){
	}

	/**
	 * @param dataset
	 * @param mean
	 * @return
	 */
	public double std(ArrayList<Double> dataset, double mean) {
		// TODO implement here
		//Sumatoria
		double sum = 0;
		for(int i = 0; i < dataset.size(); i++){
			sum += Math.pow(dataset.get(i) - mean, 2);
		}

		double total = sum / (dataset.size() - 1);
		total = Math.sqrt(total);

		return total;
	}

}
