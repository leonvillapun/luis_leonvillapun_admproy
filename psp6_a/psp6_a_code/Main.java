import java.util.*;
//Main class
public class Main{
  //Main method
  public static void main(String[] args){
    //Input of data
    Scanner input = new Scanner(System.in);

    double p = input.nextDouble();
    double dof = input.nextDouble();

    double error = 0.00001;
    double num_seg = 20;
    double d = 0.5;
    double initial_x = 1;

    ValueSearcher vsCalc = new ValueSearcher(p, dof, error, initial_x, num_seg, d);

    System.out.println(vsCalc.idealX());

  }
}
