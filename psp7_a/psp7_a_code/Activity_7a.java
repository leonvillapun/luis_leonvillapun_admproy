
import java.util.*;
import java.io.IOException;
/**
 *
 */
public class Activity_7a{

	/**
	 * Default constructor
	 */
	public Activity_7a() {
	}

	/**
	 *
	 */
	public static ArrayList<ArrayList<Double>> dataset;
	public static ArrayList<Double> dataset_x;
	public static ArrayList<Double> dataset_y;
	/**
	 *
	 */
	public static double mean;

	/**
	 *
	 */
	public static double standardDeviation;

	/**
	 * @return
	 */
	public static void main(String[] args) throws IOException{
		// TODO implement here
		try{
			Scanner input = new Scanner(System.in);
			System.out.println("Por favor, indique el nombre del archivo de donde recuperar los datos.");
			String name = input.next();
			//Importar dataset y convertirlo
			dataset = parseData(name);
			dataset_x = dataset.get(0);
			dataset_y = dataset.get(1);

			for(int i = 0; i < dataset_x.size(); i++){
				System.out.println("x: " + dataset_x.get(i) + " y: " + dataset_y.get(i));
			}


			//Ahora obtenemos la regresión lineal
			LinearRegression lrCalc = new LinearRegression();
			double[] regresion = new double[4];
			regresion = lrCalc.linearRegression(dataset_x, dataset_y);

			//Evaluamos
			System.out.println("Se ha efectuado la regresion lineal.");
			System.out.println("b0: " + regresion[1] + " b1: " + regresion[0] + " r: " + regresion[2] + " r^2: " + regresion[3]);
			System.out.println("Ingrese un valor de x para evaluar y");
			double eval = input.nextDouble();
			double y = lrCalc.predictY(regresion[1], regresion[0], eval);

			System.out.println("f(" + eval + ")= " + y);

			Significance signif = new Significance(regresion[2], dataset_x.size());
			double[] significance = signif.calculateSignificance();
			System.out.println("Significance: ");
			//System.out.println("x = " + significance[0]);
			//System.out.println("p = " + significance[1]);
			System.out.println("tail = " + significance[2]);

			double range = lrCalc.range(dataset_x, dataset_y, eval, y, dataset_x.size()-2, 0.0001, 0.35, 10, 0.5, regresion[1], regresion[0]);

			System.out.println("Range = " + range);

			System.out.println("UPI(70%) = " + (y + range));

			System.out.println("LRI(70%) = " + (y - range));
		}
		catch(Exception e){
			System.out.println("Ocurrió un error. Intente correr el programa de nuevo.");
		}
	}

	/**
	 * @return
	 */
	public static ArrayList<ArrayList<Double>> parseData(String name) throws IOException{
		// TODO implement here
		ReadFile file = new ReadFile(name);
		String[] lines = file.OpenFile();
		ArrayList<Double> lista = new ArrayList<Double>();
		ArrayList<Double> lista_y = new ArrayList<Double>();
		ArrayList<ArrayList<Double>> data = new ArrayList<ArrayList<Double>>();
		//Splitear y convertir a double
		for(int i = 0; i < lines.length; i++){
			String[] lineas = lines[i].split(" ");
			lista.add(Double.parseDouble(lineas[0]));
			lista_y.add(Double.parseDouble(lineas[1]));
		}

		data.add(lista);
		data.add(lista_y);

		return data;
	}

	/**
	 * @param dataset
	 * @return
	 */
	public static double getMean(ArrayList<Double> dataset) {
		// TODO implement here
		Mean mean = new Mean();
		return mean.mean(dataset);
	}

	/**
	 * @param dataset
	 * @param mean
	 * @return
	 */
	public static double getStd(ArrayList<Double> dataset, double mean) {
		// TODO implement here
		StandardDeviation std = new StandardDeviation();
		return std.std(dataset, mean);
	}

}
