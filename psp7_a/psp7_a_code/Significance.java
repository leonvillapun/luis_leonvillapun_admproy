import java.util.*;
public class Significance{
  //Variables globales
  double significance[] = new double[3];
  double r_xy;
  double r_xy_squared;
  double n;
  double dof;
  //Constructor
  public Significance(double r_xy, double n){
    this.r_xy = r_xy;
    this.n = n;
    this.significance = calculateSignificance();
    this.r_xy_squared = Math.pow(r_xy,2);
  }
  //Calcula la significance de una correlacion dada
  public double[] calculateSignificance(){
    double[] ans = new double[3];

    double x = (Math.abs(r_xy)*Math.sqrt(n-2)) / Math.sqrt(1 - r_xy_squared);

    Simpsons t = new Simpsons(10, 0.00001, n-2, x);

    double p = t.simpsons();

    double tail = 1-2*p;
    ans[0] = x;
    ans[1] = p;
    ans[2] = tail;
    return ans;
  }





}
