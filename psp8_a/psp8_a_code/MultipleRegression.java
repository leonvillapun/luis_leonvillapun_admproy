import java.util.*;
public class MultipleRegression{
  //Variables globales
  double[][] matrix;
  double[][] betaMatrix;
  double[][] A;
  double[] b;
  double[] empty;
  double b0, b1, b2, b3;
  double wk, xk, yk, zk;
  double[] w;
  double[] x;
  double[] y;
  double[] z;
  double range;
  double UPI;
  double LPI;
  //Constructor
  public MultipleRegression(double[][] matrix, double wk, double xk, double yk){
    this.matrix = matrix;
    betaMatrix = new double[matrix.length][matrix[0].length];
    empty = new double[matrix.length];
    for(int i = 0; i < empty.length; i++){
      empty[i] =1;
    }

    w = new double[matrix.length];
    x = new double[matrix.length];
    y = new double[matrix.length];
    z = new double[matrix.length];

    A = new double[matrix.length][matrix[0].length-1];
    for(int i = 0; i < A.length; i++){
      for(int j = 0; j < A[0].length; j++){
        A[i][j] = matrix[i][j];
        //System.out.print(A[i][j] + " ");
      }
      //System.out.println();
    }
    b = new double[matrix.length];
    for(int i = 0; i < b.length; i++){
      b[i] = matrix[i][matrix[0].length - 1];
      //System.out.println(b[i]);
    }

    System.out.println("Si pude antes del beta");
    betaMatrix = buildBetaMatrix();

    Gauss2 gauss = new Gauss2(betaMatrix);
    b0 = gauss.sol[0];
    b1 = gauss.sol[1];
    b2 = gauss.sol[2];
    b3 = gauss.sol[3];

    this.wk = wk;
    this.xk = xk;
    this.yk = yk;

    this.zk = calculateZ();

    range = range();
    System.out.println("Range: " + range);
    System.out.println("zk: " + zk);

    UPI = zk + range;
    LPI = zk - range;

    System.out.println("UPI: " + UPI);
    System.out.println("LPI: " + LPI);

  }

  public double summatory(double[] a, double[] b, int power){
    double sum = 0;
    for(int i = 0; i < a.length; i++){
      sum += (Math.pow(a[i]*b[i],power));
    }
    return sum;
  }

  public double[][] buildBetaMatrix(){
    //Primera fila
    double n = matrix.length;

    for(int i = 0; i < matrix.length; i++){
      w[i] = matrix[i][0];
      x[i] = matrix[i][1];
      y[i] = matrix[i][2];
      z[i] = matrix[i][3];
    }

    double sumW = summatory(w, empty, 1);
    double sumX = summatory(x, empty, 1);
    double sumY = summatory(y, empty, 1);
    double sumZ = summatory(b, empty, 1);
    //Segunda fila
    sumW = sumW;
    double sumWSquared = summatory(w, empty, 2);
    double sumWX = summatory(w, x,1);
    double sumWY = summatory(w, y,1);
    double sumWZ = summatory(w, b, 1);
    //Tercera fila
    sumX = sumX;
    sumWX = sumWX;
    double sumXSquared = summatory(x, empty, 2);
    double sumXY = summatory(x, y, 1);
    double sumXZ = summatory(x, b, 1);
    //Cuarta fila
    sumY = sumY;
    sumWY = sumWY;
    sumXY = sumXY;
    double sumYSquared = summatory(y, empty, 2);
    double sumYZ = summatory(y, b, 1);

    double[][] bm = {
      {n, sumW, sumX, sumY, sumZ},
      {sumW, sumWSquared, sumWX, sumWY, sumWZ},
      {sumX, sumWX, sumXSquared, sumXY, sumXZ},
      {sumY, sumWY, sumXY, sumYSquared, sumYZ}
    };
    return bm;

  }

  public double std(){
    double sum = 0;
    for(int i = 0; i < matrix.length; i++){
      sum += Math.pow(b[i] - b0 - b1*w[i] - b2*x[i] - b3*y[i],2);
    }
    double total = Math.sqrt(sum/(matrix.length - 4));
    return total;
  }

  public double range(){
    double wAvg = summatory(w, empty, 1);
    double xAvg = summatory(x, empty, 1);
    double yAvg = summatory(y, empty, 1);

    double std = std();

    ValueSearcher vs = new ValueSearcher(0.35, matrix.length - 4, 0.00001, 1, 10, 0.5);
    double t = vs.idealX();

    double sumsW = 0;
    double sumsX = 0;
    double sumsY = 0;
    for(int i = 0; i < matrix.length; i++){
      sumsW += Math.pow(w[i]-wAvg,2);
      sumsX += Math.pow(x[i]-xAvg,2);
      sumsY += Math.pow(y[i]-yAvg,2);
    }

    double lastPart = Math.sqrt(1+(1/matrix.length)+(Math.pow(wk-wAvg,2)/sumsW)+(Math.pow(xk-xAvg,2)/sumsX)+(Math.pow(yk-yAvg,2)/sumsY));

    return t*std*lastPart;
  }

  public double calculateZ(){
    return b0 + b1*wk + b2*xk + b3*yk;
  }

}
