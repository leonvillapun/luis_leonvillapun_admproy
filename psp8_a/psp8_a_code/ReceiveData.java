import java.util.*;
public class ReceiveData{
  public static void main(String[] args){
    try{
      //Input de datos
      Scanner input = new Scanner(System.in);
      System.out.println("Enter number of classes");
      double classes = input.nextDouble();
      System.out.println("Enter number of data");
      double data = input.nextDouble();

      double[][] matrix = new double[(int)data][(int)classes];

      for(int i = 0; i < data; i++){
        for(int j = 0; j < classes; j++){
          matrix[i][j] = input.nextDouble();
        }
      }
      System.out.println("SI PUDE CREAR LA MATRIZ");
      //
      //Procesarlos
      System.out.println("Enter added code:");
      double ac = input.nextDouble();
      System.out.println("Enter reused code:");
      double rc = input.nextDouble();
      System.out.println("Enter modified code:");
      double mc = input.nextDouble();
      MultipleRegression mr = new MultipleRegression(matrix, ac, rc, mc);
    }
    catch(Exception e){
      System.out.println("Ocurrió un error. Por favor, vuelva a correr el programa.");
    }
  }
}
